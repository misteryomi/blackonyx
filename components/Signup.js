import React, { Component } from 'react';
import { Image } from 'react-native';
import { Content, Text, Input, Item, Container, Header, Title, Subtitle, Body, Right, Left, Label, Icon, View, Button, H2 } from 'native-base';
import Main from './inc/Main';

export default class Signup extends Component {

    render() {

        return (
            <Container>
            <Content padder>
                <View style={{ marginTop: 5, alignItems: 'center'}}>
                    <H2>Create an Account</H2>
                    <Text style={{marginTop: 2, fontSize: 12 }}>Create an account by filling the form below</Text>
                </View>

                <View style={{ marginTop: 7}}>
                    <Label style={{fontSize: 12, paddingBottom: 5}}>Fullname</Label>
                    <Item regular>
                        <Icon active name='person' />
                        <Input  style={{fontSize: 12}}  placeholder='John Doe' />
                    </Item>
                </View>
                <View style={{ marginTop: 15}}>
                    <Label style={{fontSize: 12, paddingBottom: 5}}>Email</Label>
                    <Item regular>
                        <Icon active name='mail' />
                        <Input style={{fontSize: 12}} placeholder='Johndoe@gmail.com' />
                    </Item>
                </View>
                <View style={{ marginTop: 15}}>
                    <Label style={{fontSize: 12, paddingBottom: 5}}>Password</Label>
                    <Item regular>
                        <Icon active name='lock' />
                        <Input  style={{fontSize: 12}}  placeholder='*******' />
                    </Item>
                </View>
                <View style={{ marginTop: 15}}>
                    <Text style={{fontSize: 12, marginLeft: 40, marginRight: 40, textAlign: 'center' }}>By creating account, you agree to our Terms of Service and Privacy Policy</Text>
                </View>
                <View style={{ marginTop: 15, marginBottom: 10, textAlign: 'center'}}>                        
                    <Button block warning onPress={ () => this.props.navigation.navigate('Home')}>
                        <Text style={{ padding: 10}}>Sign Up</Text>
                    </Button>
                </View>
                <View style={{ marginTop: 5, justifyContent: 'center',  alignItems: 'center', height: 100 }}>
                    <Text style={{fontSize: 12, textAlign: 'center' }}>or signup with</Text>

                    <Image source={ require('./images/socials.png')} style={{marginTop: 10, height: 41, width: 150 }}/>
                </View>
                <View style={{ marginTop: 5, marginBottom: 15, textAlign: 'center'}}>                        
                    <Text style={{fontSize: 12, textAlign: 'center' }}>Already have an account?</Text>
                    <Button block bordered style={{marginTop: 20, padding: 10}} dark onPress={ () => this.props.navigation.navigate('Login')}>
                        <Text style={{ padding: 10}}>Login</Text>
                    </Button>
                </View>

            </Content>
          </Container>              
        );
  }
}