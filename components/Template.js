import React, { Component } from 'react';
import { Content, Text } from 'native-base';
import Main from './inc/Main';

export default class Articles extends Component {

    render() {
        this.content = (
            <Content>
              <Text>Hello</Text>
            </Content>
        );

        return (
          <Main body={this.content}/>
    );
  }
}