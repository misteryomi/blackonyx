import React from 'react';
import { Drawer, Header, Left, Right, Button, Icon, Body, Title, View } from 'native-base';
import Sidebar from './Sidebar';

export default class HeaderTemplate extends React.Component {

    render() {
        return (
            <View>
                <Header style={{backgroundColor: '#000'}} titleStyle={{textAlign: 'center'}}>
                    <Left>
                        <Button transparent onPress={this.props.openDrawer}>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{textAlign: 'center'}}>{this.props.title}</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon name='ios-notifications' />
                        </Button>
                    </Right>
                </Header>
            </View>

        )
    }
}