import React, { Component } from 'react';
import { Drawer, Container, Content, Text, Button, View } from 'native-base';
import Sidebar from './Sidebar';
import HeaderTemplate from './HeaderTemplate';
import FooterTemplate from './FooterTemplate';
import { withNavigation } from 'react-navigation';

import { createStackNavigator, createAppContainer } from 'react-navigation';

class Main extends Component {


  closeDrawer = () => {
      this.drawer._root.close();
  }

  openDrawer = () => {
      this.drawer._root.open();
  }
  navigate = (v) => {
    this.props.navigation.navigate(v);
  }




    render() {
        return (
            <Drawer
                type="overlay"
                
                ref={ (ref) => {this.drawer = ref; }}                    
                // content={<Sidebar />}
                onClose={ () => this.closeDrawer()}
                styles={ {  drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3}}}
                tweenHandler={(ratio) => ({
                    main: { opacity:(2-ratio)/2 },
                    mainOverlay: { opacity: ratio/1.5, backgroundColor: 'black' }
                  })}            
                  
                >
          <HeaderTemplate title={this.props.title} openDrawer={this.openDrawer.bind(this)}/>
          {this.props.body}
          <FooterTemplate navigate={this.navigate.bind(this)}/>
        </Drawer>
    );
  }
}

export default withNavigation(Main);