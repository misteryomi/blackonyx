import React from 'react'
import {  Footer, FooterTab, Button, Icon, Text, View } from 'native-base';
import NavigationService from '../../NavigationService';


export default class FooterTemplate extends React.Component {

    render() {
        return (
            <View>
                 <Footer>
                    <FooterTab style={{backgroundColor: '#000'}}>
                        <Button vertical onPress={() => this.props.navigate('Home')}>
                        <Icon name="grid" />
                        <Text>Home</Text>
                        </Button>
                        <Button vertical>
                        <Icon name="star" />
                        <Text>Favorites</Text>
                        </Button>
                        <Button vertical onPress={() => this.props.navigate('Articles')}>
                        <Icon name="book" />
                        <Text>Articles</Text>
                        </Button>
                        <Button vertical>
                        <Icon name="person" />
                        <Text>Profile</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </View>

        )
    }
}