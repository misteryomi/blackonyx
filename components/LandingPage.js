import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';
import { Content, Text, Container, Footer, Button, View, H1, Grid, Row, Col } from 'native-base';
import { withNavigation } from 'react-navigation';

import Swiper from 'react-native-swiper';

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  }
})

class LandingPage extends Component {

    render() {
        return (

           <Swiper 
                    dot={<View style={{backgroundColor: 'rgba(112,112,112,.3)', width: 5, height: 5, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
                    activeDot={<View style={{backgroundColor: '#000', width: 5, height: 5, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
                    paginationStyle={{
                        bottom: 170
                    }}
                    style={styles.wrapper}
                     showsButtons={false} >
                <View style={styles.slide1}>    
                    <View style={{ flex: 6, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                         <Image source={ require('./images/discover.png')}  style={{height: 250, width: 400}} />
                         <H1 style={{ padding: 10, textAlign: 'center', fontSize: 30}}>Discover</H1>
                         <View style={{width: '80%'}}>
                             <Text style={{ padding: 10,  fontSize: 12, textAlign: 'center'}}>You can discover properties for sale or rent, post properties for sale or rent and contact sellers and buyers directly from our app</Text>
                         </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'stretch' }}>
                        <View>
                            <Button vertical bordered dark style={{ paddingRight: 25, paddingLeft: 25, marginRight: 10}}>
                                <Text style={{ padding: 10, paddingRight: 25, paddingLeft: 25}}>SKIP</Text>
                            </Button>
                        </View>
                        <View>
                            <Button vertical warning noShadow  style={{ paddingRight: 25, paddingLeft: 25, marginRight: 10}} onPress={ () => this.props.navigation.navigate('Login')}>
                                <Text style={{ padding: 10, paddingRight: 25, paddingLeft: 25, marginLeft: 10}}>NEXT</Text>
                            </Button>
                        </View>
                    </View>
                </View>
                <View style={styles.slide2}>
                    <View style={{ flex: 7, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'  }}>
                         <Image source={ require('./images/articles.png')}  style={{height: 250, width: 400, textAlign: 'center'}} />
                         <H1 style={{ padding: 10, textAlign: 'center', fontSize: 30}}>Articles</H1>
                         <View style={{width: '80%'}}>
                             <Text style={{ padding: 10,  fontSize: 12, textAlign: 'center'}}>Read the latest articles around the nation about
real estate and properties, get insight and more directly from our app</Text>
                         </View>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'stretch', width: '90%'  }}>
                        
                            <Button block warning onPress={ () => this.props.navigation.navigate('Login')}>
                                <Text style={{ padding: 10}}>LET'S START</Text>
                            </Button>
                    </View>
                </View>
          </Swiper>

            // <Container>
            //     <Content>
            //         <View style={{  marginTop: 100 }} >
            //             <Image source={ require('./images/discover.png')}  style={{height: 300, width: '90%'}} />
            //             <H1 style={{ padding: 10, textAlign: 'center', fontSize: 40}}>Discover</H1>
            //             <Text style={{ padding: 10,  textAlign: 'center',}}>You can discover properties for sale or rent, post properties for sale or rent and contact sellers and buyers directly from our app</Text>
            //         </View>
            //         <View style={{marginTop: 100, width: '100%', flex: 1, justifyContent: 'space-around'}}>
            //         <Button vertical bordered dark style={{padding: 10}}>
            //             <Text style={{ padding: 10, paddingRight: 25, paddingLeft: 25, marginRight: 10}}>SKIP</Text>
            //         </Button>
            //         <Button vertical warning style={{padding: 10}} onPress={ () => this.props.navigation.navigate('Home')}>
            //             <Text style={{ padding: 10, paddingRight: 25, paddingLeft: 25, marginLeft: 10}}>NEXT</Text>
            //         </Button>

            //     </View>

            //     </Content>
            // </Container>

            // <View  style={{
            //         flex: 1,
            //         flexDirection: 'column',
            //         justifyContent: 'center',
            //         alignItems: 'center',
            //     }}> 
            //      <Image source={ require('./images/discover.png')} style={{height: 300, width: '90%'}}/>
            //     <View style={{textAlign: 'center'}}>
            //             <H1 style={{ padding: 10, textAlign: 'center', fontSize: 40}}>Discover</H1>
            //             <Text style={{ padding: 10, textAlign: 'center',}}>You can discover properties for sale or rent, post properties for sale or rent and contact sellers and buyers directly from our app</Text>
            //     </View>

            //     <View style={{height: 100}} >
            //         <Button vertical bordered dark style={{padding: 10}}>
            //             <Text>Apps</Text>
            //         </Button>
            //         <Button vertical warning style={{padding: 10}}>
            //             <Text>Apps</Text>
            //         </Button>
            //     </View>
            // </View>
        );
  }
}
export default withNavigation(LandingPage);