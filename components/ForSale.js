import React, { Component } from 'react';
import{ Image } from 'react-native';
import { Content, Text, Card, CardItem, Body, Grid, Col, H3, Right, Icon, Row, Button, Left } from 'native-base';
import Main from './inc/Main';


export default class ForSale extends Component {

    render() {

        const items = [
            {
                price: 'N100,000',
                description: 'Georgeous 2 Bedroom Semi Detached Duplex +  2 Penthouse Rooms At Ikoyi',
                image: require('./images/image.png')
            },
            {
                price: 'N500,000',
                description: 'Georgeous 2 Bedroom Semi Detached Duplex +  2 Penthouse Rooms At Ikoyi',
                image: require('./images/image.png')
            },
            {
                price: 'N150,000',
                description: 'Georgeous 2 Bedroom Semi Detached Duplex +  2 Penthouse Rooms At Ikoyi',
                image: require('./images/image.png')
            },
            {
                price: 'N80,000',
                description: 'Georgeous 2 Bedroom Semi Detached Duplex +  2 Penthouse Rooms At Ikoyi',
                image: require('./images/image.png')
            },
        ]

        const content = (
            <Content>
                {
                    items.map((item, index) => {
                        return (
                        <Card key={index}>
                            <CardItem style={{ padding: 0}} >
                            <Body>
                                <Grid>
                                    <Col style={{ height: 160, width:160, paddingRight: 10 }} >
                                    <Image 
                                        source={ item.image } 
                                        style={{height: '100%', width: '100%'}}
                                        />
                                    </Col>
                                    <Col>
                                        <Row style={{height: 30}}>
                                            <Col>
                                                    <H3 style={{fontSize: 14, fontWeight: 'bold'}}>
                                                        <Icon style={{fontSize: 13, paddingRight: 10}}   type='Entypo' name='price-tag' />
                                                        {item.price}
                                                    </H3>
                                            </Col>
                                            <Col style={{width: 20}}>
                                                <Icon style={{fontSize: 12}} type='Entypo' name='location-pin' />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Text style={{fontSize: 12}}>{item.description}</Text>
                                            </Col>
                                        </Row>
                                        <Row style={{ textAlign: 'center', marginTop: 15}}>
                                            <Col bordered style={{ borderTop: '1px solid #000', textAlign: 'center'}}>
                                                <Icon style={{fontSize: 20, textAlign: 'center'}} type='FontAwesome' name='bathtub' />
                                                <Text style={{ textAlign: 'center', fontSize: 12 }}>2{'\n'}Bathroom</Text>
                                            </Col>
                                            <Col style={{ borderTop: '1px solid #000', textAlign: 'center'}}>
                                                <Icon style={{fontSize: 20, textAlign: 'center'}} name='ios-bed' />
                                                <Text style={{ textAlign: 'center', fontSize: 12 }}>2{'\n'}Bedroom</Text>
                                            </Col>
                                            <Col style={{ borderTop: '1px solid #000', textAlign: 'center'}}>
                                                <Icon style={{fontSize: 20, textAlign: 'center'}} name='ios-car' />
                                                <Text style={{ textAlign: 'center', fontSize: 12 }}>2{'\n'}Car space</Text>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Text style={{ paddingTop: 5, textAlign: 'right', color: '#FBC926', fontSize: 10}}>
                                                    Add to favorites
                                                    <Icon  style={{paddingLeft: 10, textAlign: 'right', color: '#FBC926', fontSize: 12}} name="star" />
                                                </Text>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Grid>
                            </Body>
                            </CardItem>
                        </Card>
                        )
                    })
                }
            </Content>
        );

        return (
          <Main body={content} title='Sale'/>
    );
  }
}