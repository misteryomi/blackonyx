import React, { Component } from 'react';
import {Image, ImageBackground, Dimensions} from 'react-native';
import { Content, Text, H1, Grid, Col, Row, View, Button } from 'native-base';
import { withNavigation } from 'react-navigation';
import Main from './inc/Main';

const { width, height } = Dimensions.get('window')

class Home extends Component {

    render() {
        this.content = (
            <Content>
              <ImageBackground source={ require('./images/bg.png')} style={{flex: 1, width: '100%'}} resizeMode= 'stretch'>
              <View style={{padding: 10, flex: 1}}>
                  <View style={{flex: 1}}>
                    <H1 style={{fontSize: 25, fontWeight: 'bold', lineHeight: 30, marginTop: 20}}>What are you{'\n'}looking for?</H1>
                    <Text style={{ paddingTop: 10, paddingBottom: 30, fontSize: 12, width: 300}}>Get the best deals on properties and the latest insights in property management with just a click</Text>
                  </View>
                  <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                      <View style={{height: 180, width: '48%' }}>
                        <Button transparent onPress={() => this.props.navigation.navigate('ForSale')} style={{height: '100%', width: '100%'}}>
                        <Image 
                              source={ require('./images/forsale.png') } 
                              style={{height: '100%', width: '100%'}}
                              />
                        </Button>
                      </View>
                      <View style={{height: 180, width: '48%' }}>
                        <Button transparent onPress={() => this.props.navigation.navigate('ForRent')} style={{height: '100%', width: '100%'}}>
                        <Image 
                              source={ require('./images/forrent.png') } 
                              style={{height: '100%', width: '100%'}}
                              />
                        </Button>
                      </View>
                  </View>
                  <View style={{flex: 2, flexDirection: 'row', justifyContent: 'space-around'}}>
                    <View style={{height: 180, width: '100%'}}>
                      <Button style={{flex: 1}} transparent onPress={() => this.props.navigation.navigate('Articles')} style={{height: '100%', width: '100%'}}>
                      <Image 
                          source={ require('./images/home_articles.png') } 
                          style={{height: '100%', width: '100%'}}
                          button
                          onPress={() => this.props.navigation.navigate('Articles')}                    
                          />
                      </Button>
                      </View>
                  </View>
                </View>
              {/* <Grid style={{marginTop: 10}}>
                <Row>
                <Col style={{height: 200, paddingRight: 7 }}>
                  <Button style={{flex: 1}} transparent onPress={() => this.props.navigation.navigate('ForSale')} >
                  <Image 
                        source={ require('./images/forsale.png') } 
                        style={{height: '100%', width: '100%'}}
                        />
                   </Button>
                </Col>
                <Col style={{height: 200, paddingLeft: 7 }}>
                  <Button style={{flex: 1}} transparent onPress={() => this.props.navigation.navigate('ForRent')} >
                  <Image 
                      source={ require('./images/forrent.png') } 
                      style={{height: '100%', width: '100%'}}
                      button
                      onPress={() => this.props.navigation.navigate('ForRent')}                    
                      />
                  </Button>
                                        
                </Col>
                </Row>                
              <Row style={{marginTop: 15}}>
                <Col style={{ height: 200 }} >
                  <Button style={{flex: 1}} transparent onPress={() => this.props.navigation.navigate('Articles')} >
                  <Image 
                      source={ require('./images/home_articles.png') } 
                      style={{height: '100%', width: '100%'}}
                      button
                      onPress={() => this.props.navigation.navigate('Articles')}                    
                      />
                   </Button>
                </Col>
                </Row>
              </Grid> */}
              </ImageBackground>
            </Content>
        );

        return (
          <Main body={this.content} title='Home'/>
    );
  }
}

export default withNavigation(Home);