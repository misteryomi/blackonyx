import React, { Component } from 'react';
import {Image} from 'react-native';
import { Content, Text, Card, CardItem, Left, Right, Body, Icon, Button, H2, P } from 'native-base';
import Main from './inc/Main';
import { withNavigation } from 'react-navigation';
import ArticleDetails from './ArticleDetails';

class Articles extends Component {
  state = {
    navigateTo: ''
  }

  navigate = (v) => {
    this.props.navigation.navigate(v);
  }


  render() {
        const articles = [
          {
            title: '2017 Year in Review - Nigeria Property Center',
            body: 'Nigeria Property Center, January 30, 2018 - Nigeria Property Center (NPC), a leading property search website in Nigeria has announced its...',
            image: require('./images/article1.jpg'),
            time: '1h ago',
          },
          {
            title: 'Becoming the Estate Agent of Value in Nigeria',
            body: 'Without boring you with dictionary definitions, estate agency is a fiduciary relationship whereby a person (referred to as ‘an estate agent’) ...',
            image: require('./images/article2.jpg'),
            time: '1h ago',
          },
          {
            title: 'Please Rein In Omoniles and Corrupt, Overzealous..',
            body: 'If we must move forward as a nation, we need to whittle down a penchant for raw use of power, coercion and extortion. A while ago it...',
            image: require('./images/article3.jpg'),
            time: '1h ago',
          },
        ]
  
        this.content = (
            <Content>
              {articles.map( (article, index) => {
                return (
                   <Card key={index} >
                <CardItem button onPress={() => this.navigate('ArticleDetails')} >
                  <Body>
                    <Image source={ article.image } style={{height: 150, width: '100%', flex: 1 }}/>
                    <H2 style={{fontWeight: 'bold', fontSize: 14, marginTop: 10}}>{article.title}</H2>
                    <Text style={{fontSize: 12}}>{article.body}</Text>
                  </Body>
                </CardItem>
                <CardItem>
                  <Left>
                    <Button transparent>
                      <Icon  style={{fontSize: 12}} active name="ios-clock" />
                      <Text style={{fontSize: 12}}>{article.time}</Text>
                    </Button>
                  </Left>
                  <Body>
                  </Body>
                  <Right>
                    <Button small transparent>
                      <Icon  style={{fontSize: 12}} active name="share" />
                    </Button>
                  </Right>
                </CardItem>
              </Card>
                )
              })}
            </Content>
        );

        return (
          <Main body={this.content} navigateTo={this.state.navigateTo} title="Articles"/>
    );
  }
}

export default withNavigation(Articles);