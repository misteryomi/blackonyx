import React, { Component } from 'react';
import { Image } from 'react-native';
import { Content, Text, Input, Item, Container, Header, Title, Subtitle, Body, Right, Left, Label, Icon, View, Button, H2 } from 'native-base';
import Main from './inc/Main';

export default class Login extends Component {

    render() {

        return (
            <Container>
            <Content padder>
                <View style={{ marginTop: 5, alignItems: 'center'}}>
                    <H2>Login</H2>
                    <Text style={{marginTop: 5, fontSize: 12 }}>Login to your account by filling the form below</Text>
                </View>

                <View style={{ marginTop: 10}}>
                    <Label style={{fontSize: 12, paddingBottom: 5}}>Email</Label>
                    <Item regular>
                        <Icon active name='mail' />
                        <Input  style={{fontSize: 12}} placeholder='Johndoe@gmail.com' />
                    </Item>
                </View>
                <View style={{ marginTop: 15}}>
                    <Label style={{fontSize: 12, paddingBottom: 5}}>Password</Label>
                    <Item regular>
                        <Icon active name='lock' />
                        <Input  style={{fontSize: 12}} placeholder='*******' />
                    </Item>
                </View>
                <View style={{ marginTop: 15}}>
                    <Text style={{fontSize: 12, marginLeft: 40, marginRight: 40, textAlign: 'center' }}>By logging into account, you agree to our Terms of Service and Privacy Policy</Text>
                </View>
                <View style={{ marginTop: 15, marginBottom: 15, textAlign: 'center'}}>                        
                    <Button block warning onPress={ () => this.props.navigation.navigate('Home')}>
                        <Text style={{ padding: 10}}>Login</Text>
                    </Button>
                </View>
                <View style={{ marginTop: 10, justifyContent: 'center',  alignItems: 'center', height: 120 }}>
                    <Text style={{fontSize: 12, textAlign: 'center' }}>or login with</Text>

                    <Image source={ require('./images/socials.png')} style={{marginTop: 10, height: 61, width: 220 }}/>
                </View>
                <View style={{ marginTop: 10, marginBottom: 15, textAlign: 'center'}}>                        
                    <Text style={{fontSize: 12, textAlign: 'center' }}>Don't have an account?</Text>
                    <Button block bordered style={{marginTop: 20, padding: 10}} dark onPress={ () => this.props.navigation.navigate('Signup')}>
                        <Text style={{ padding: 10}}>Sign Up</Text>
                    </Button>
                </View>

            </Content>
          </Container>            
        );
  }
}