import {Navigation} from 'react-native-navigation';

export function registerScreens() {
  Navigation.registerComponent('Home', () => require('./components/Home').default);
  Navigation.registerComponent('Articles', (sc) => require('./components/Articles').default);
//   Navigation.registerComponent('SignIn', () => require('./SignIn').default);
//   Navigation.registerComponent('SignUp', () => require('./SignUp').default);
//   Navigation.registerComponent('Screen2', () => require('./Screen2').default);
}