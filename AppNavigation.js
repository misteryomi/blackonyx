import {createStackNavigator, createAppContainer} from 'react-navigation';
import Home from './components/Home';
import Articles from './components/Articles';
import ForRent from './components/ForRent';
import ForSale from './components/ForSale';
import ArticleDetails from './components/ArticleDetails';
import LandingPage from './components/LandingPage';
import Login from './components/Login';
import Signup from './components/Signup';

const AppNavigator = createStackNavigator({
                            LandingPage: LandingPage,
                            Home: Home,
                            Signup: Signup,
                            Articles: Articles,
                            ForSale: ForSale,
                            ForRent: ForRent,
                            Login: Login,
                            ArticleDetails: ArticleDetails,
                        },
                        { headerMode: 'none'},
                        {
                            header: {
                                titleStyle: {
                                   textAlign: 'center',
                                },
                             },
                            },
                            {
                            initialRouteName: "Home"
                        },
                        );

export default createAppContainer(AppNavigator)
