import React from 'react';
import NavigationService from './NavigationService';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import AppNavigation from './AppNavigation';

import { AppLoading } from 'expo';

export default class App extends React.Component {
  
  state = {
    loading: true 
  }
  
  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState({loading: false});
  }

  render() {
    if(this.state.loading) {
      return <AppLoading />
    }

    return (
      <AppNavigation />
    );
  }
}
